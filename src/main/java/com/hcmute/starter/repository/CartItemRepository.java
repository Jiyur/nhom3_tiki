package com.hcmute.starter.repository;

import com.hcmute.starter.model.entity.CartItemEntity;
import com.hcmute.starter.model.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.Optional;
@EnableJpaRepositories
public interface CartItemRepository extends JpaRepository<CartItemEntity,Integer> {
    Optional<CartItemEntity> findByProduct(ProductEntity product);
}
