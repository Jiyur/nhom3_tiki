package com.hcmute.starter.controller;

import com.hcmute.starter.mapping.UserMapping;
import com.hcmute.starter.model.entity.UserEntity;
import com.hcmute.starter.model.payload.SuccessResponse;
import com.hcmute.starter.model.payload.request.UserRequest.*;
import com.hcmute.starter.security.JWT.JwtUtils;
import com.hcmute.starter.service.CountryService;
import com.hcmute.starter.service.ImageStorageService;
import com.hcmute.starter.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import static com.google.common.net.HttpHeaders.AUTHORIZATION;

@RestController
@RequestMapping("api/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    private final CountryService countryService;
    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    private final ImageStorageService imageStorageService;

    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    JwtUtils jwtUtils;
    //Get user by id
    //This request is: http://localhost:8080/api/user/{id}
    @GetMapping("{id}")
    public ResponseEntity<SuccessResponse> getUserByID(@PathVariable("id")String id) {
        UserEntity user=userService.findById(UUID.fromString(id));
        SuccessResponse response=new SuccessResponse();
        if(user==null){
            response.setMessage("User not found");
            response.setStatus(HttpStatus.NOT_FOUND.value());
            response.setSuccess(false);
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);

        }
        else{
            response.setMessage("Get user success");
            response.setSuccess(true);
            response.setStatus(HttpStatus.OK.value());
            response.getData().put("user",user);
            return new ResponseEntity<>(response, HttpStatus.OK);

        }

    }
    //This request is: http://localhost:8080/api/user/profile
    @GetMapping("/profile")
    public ResponseEntity<SuccessResponse> getUserProfile(HttpServletRequest request) throws Exception{
        String authorizationHeader = request.getHeader(AUTHORIZATION);
        if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            String accessToken = authorizationHeader.substring("Bearer ".length());
            if(jwtUtils.validateExpiredToken(accessToken)){
                throw new BadCredentialsException("access token is  expired");
            }
            UserEntity user=userService.findById(UUID.fromString(jwtUtils.getUserNameFromJwtToken(accessToken)));
            if(user.getId().toString().length()<1){
                throw new BadCredentialsException("User not found");
            }
            else{
                SuccessResponse response=new SuccessResponse();
                response.setMessage("Get user success");
                response.setSuccess(true);
                response.setStatus(HttpStatus.OK.value());
                response.getData().put("user",user);
                return new ResponseEntity<>(response, HttpStatus.OK);
            }


        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);


    }
    //register user API
    @PostMapping("/register")
    public ResponseEntity<SuccessResponse> registerAccount(@RequestBody @Valid AddNewUserRequest request) {
        UserEntity user= UserMapping.registerToEntity(request);
        if(userService.existsByPhone(user.getPhone())){
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        SuccessResponse response=new SuccessResponse();
        try{
            userService.saveUser(user,"USER");
            response.setStatus(HttpStatus.OK.value());
            response.setMessage("add user successful");
            response.setSuccess(true);
            response.getData().put("phone",user.getPhone());
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return new ResponseEntity<>(response, HttpStatus.OK);

    }
    //upload user's avatar API
    @PostMapping("/profile/uploadAvatar")
    public ResponseEntity<SuccessResponse> uploadAvatar(HttpServletRequest request, @RequestPart MultipartFile file) throws Exception{
        String authorizationHeader = request.getHeader(AUTHORIZATION);
        if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            String accessToken = authorizationHeader.substring("Bearer ".length());
            if(jwtUtils.validateExpiredToken(accessToken)){
                throw new BadCredentialsException("access token is  expired");
            }
            UserEntity user=userService.findById(UUID.fromString(jwtUtils.getUserNameFromJwtToken(accessToken)));
            if(user==null){
                throw new BadCredentialsException("User not found");
            }
            else{
                if(!imageStorageService.isImageFile(file)){
                    SuccessResponse response=new SuccessResponse();
                    response.setMessage("The file is not an image");
                    response.setSuccess(false);
                    response.setStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE.value());
                    return new ResponseEntity<>(response, HttpStatus.UNSUPPORTED_MEDIA_TYPE);
                }
                String url = imageStorageService.saveAvatar(file,user.getPhone());
                if(url.equals("")){
                    SuccessResponse response=new SuccessResponse();
                    response.setMessage("Upload Avatar Failure");
                    response.setSuccess(false);
                    response.setStatus(HttpStatus.NOT_FOUND.value());
                    return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
                }
                user.setImg(url);
                userService.saveInfo(user);
                SuccessResponse response=new SuccessResponse();
                response.setMessage("Upload Avatar successfully");
                response.setSuccess(true);
                response.setStatus(HttpStatus.OK.value());
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }
    //password change API
    @PutMapping("/profile/changePassword")
    public ResponseEntity<SuccessResponse> changePassword(HttpServletRequest req, @RequestBody @Valid ChangePasswordRequest request) throws Exception{
        String authorizationHeader = req.getHeader(AUTHORIZATION);
        if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            String accessToken = authorizationHeader.substring("Bearer ".length());
            if(jwtUtils.validateExpiredToken(accessToken)){
                throw new BadCredentialsException("access token is  expired");
            }
            UserEntity user=userService.findById(UUID.fromString(jwtUtils.getUserNameFromJwtToken(accessToken)));
            if(user==null){
                throw new BadCredentialsException("User not found");
            }
            else{
                if(!passwordEncoder.matches(request.getOldPassword(),user.getPassword())){
                    SuccessResponse response=new SuccessResponse();
                    response.setMessage("Old password is incorrect");
                    response.setSuccess(false);
                    response.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
                    return new ResponseEntity<>(response, HttpStatus.UNPROCESSABLE_ENTITY);
                }
                if(!request.getNewPassword().equals(request.getConfirmPassword())){
                    SuccessResponse response=new SuccessResponse();
                    response.setMessage("New password is not the same as confirm password");
                    response.setSuccess(false);
                    response.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
                    return new ResponseEntity<>(response, HttpStatus.UNPROCESSABLE_ENTITY);
                }
                user.setPassword(passwordEncoder.encode(request.getNewPassword()));
                userService.saveInfo(user);
                SuccessResponse response=new SuccessResponse();
                response.setMessage("Change password successfully");
                response.setSuccess(true);
                response.setStatus(HttpStatus.OK.value());
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }
    //change user info API
    @PutMapping("/profile/changeInfo")
    public ResponseEntity<SuccessResponse> changeInfo(HttpServletRequest req, @RequestBody @Valid ChangeInfoRequest request) throws Exception{
        String authorizationHeader = req.getHeader(AUTHORIZATION);
        if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            String accessToken = authorizationHeader.substring("Bearer ".length());
            if(jwtUtils.validateExpiredToken(accessToken)){
                throw new BadCredentialsException("access token is expired");
            }
            UserEntity user=userService.findById(UUID.fromString(jwtUtils.getUserNameFromJwtToken(accessToken)));
            if(user==null){
                throw new BadCredentialsException("User not found");
            }
            else{
                user.setFullName(request.getFullName());
                user.setGender(request.getGender());
                user.setNickName(request.getNickName());
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                LocalDateTime dateTime = LocalDateTime.parse(request.getBirthDay()+" 00:00", formatter);
                user.setBirth_day(dateTime);
                user.setCountry(countryService.findById(request.getCountry()));
                userService.saveInfo(user);
                SuccessResponse response=new SuccessResponse();
                response.setMessage("Change info successfully");
                response.setSuccess(true);
                response.setStatus(HttpStatus.OK.value());
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }
    @PutMapping("/profile/changeEmail")
    public ResponseEntity<SuccessResponse> changeEmail(HttpServletRequest req, @RequestBody @Valid ChangeEmailRequest request) throws Exception{
        String authorizationHeader = req.getHeader(AUTHORIZATION);
        if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            String accessToken = authorizationHeader.substring("Bearer ".length());
            if(jwtUtils.validateExpiredToken(accessToken)){
                throw new BadCredentialsException("access token is expired");
            }
            UserEntity user=userService.findById(UUID.fromString(jwtUtils.getUserNameFromJwtToken(accessToken)));
            if(user==null){
                throw new BadCredentialsException("User not found");
            }
            else{
                if(userService.findByEmail(request.getEmail())!=null){
                    SuccessResponse response=new SuccessResponse();
                    response.setMessage("Email is already used");
                    response.setSuccess(false);
                    response.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
                    return new ResponseEntity<>(response, HttpStatus.UNPROCESSABLE_ENTITY);
                }
                user.setEmail(request.getEmail());
                userService.saveInfo(user);
                SuccessResponse response=new SuccessResponse();
                response.setMessage("Change email successfully");
                response.setSuccess(true);
                response.setStatus(HttpStatus.OK.value());
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }
    @PutMapping("/profile/changePhone")
    public ResponseEntity<SuccessResponse> changePhone(HttpServletRequest req, @RequestBody ChangePhoneRequest request) throws Exception{
        String authorizationHeader = req.getHeader(AUTHORIZATION);
        if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            String accessToken = authorizationHeader.substring("Bearer ".length());
            if(jwtUtils.validateExpiredToken(accessToken)){
                throw new BadCredentialsException("access token is expired");
            }
            UserEntity user=userService.findById(UUID.fromString(jwtUtils.getUserNameFromJwtToken(accessToken)));
            if(user==null){
                throw new BadCredentialsException("User not found");
            }
            else{
                if(userService.findByPhone(request.getPhone())!=null ){
                    SuccessResponse response=new SuccessResponse();
                    response.setMessage("Phone is already used");
                    response.setSuccess(false);
                    response.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
                    return new ResponseEntity<>(response, HttpStatus.UNPROCESSABLE_ENTITY);
                }
                user.setPhone(request.getPhone());
                userService.saveInfo(user);
                SuccessResponse response=new SuccessResponse();
                response.setMessage("Change phone successfully");
                response.setSuccess(true);
                response.setStatus(HttpStatus.OK.value());
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }
    @GetMapping("/wishlist")
    public ResponseEntity<SuccessResponse> getWishlist(HttpServletRequest req) throws Exception{
        String authorizationHeader = req.getHeader(AUTHORIZATION);
        if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            String accessToken = authorizationHeader.substring("Bearer ".length());
            if(jwtUtils.validateExpiredToken(accessToken)){
                throw new BadCredentialsException("access token is expired");
            }
            UserEntity user=userService.findById(UUID.fromString(jwtUtils.getUserNameFromJwtToken(accessToken)));
            if(user==null){
                throw new BadCredentialsException("User not found");
            }
            else{
                SuccessResponse response=new SuccessResponse();
                response.setMessage("Get wishlist successfully");
                response.setSuccess(true);
                response.setStatus(HttpStatus.OK.value());
                response.getData().put("user",user.getFavoriteProducts());
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

    }
//    @DeleteMapping("/wishlist/delete/{id}")
//    public ResponseEntity<SuccessResponse> deleteWishlist(HttpServletRequest req,@PathVariable("id") String id) throws Exception{
//        String authorizationHeader = req.getHeader(AUTHORIZATION);
//        if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
//            String accessToken = authorizationHeader.substring("Bearer ".length());
//            if(jwtUtils.validateExpiredToken(accessToken)){
//                throw new BadCredentialsException("access token is expired");
//            }
//            UserEntity user=userService.findById(UUID.fromString(jwtUtils.getUserNameFromJwtToken(accessToken)));
//            if(user==null){
//                throw new BadCredentialsException("User not found");
//            }
//            else{
//                user.getFavoriteProducts().remove(productService.findById(UUID.fromString(id)));
//                userService.saveInfo(user);
//                SuccessResponse response=new SuccessResponse();
//                response.setMessage("Delete wishlist successfully");
//                response.setSuccess(true);
//                response.setStatus(HttpStatus.OK.value());
//                return new ResponseEntity<>(response, HttpStatus.OK);
//            }
//        }
//        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
//    }
}
