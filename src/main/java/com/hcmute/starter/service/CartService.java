package com.hcmute.starter.service;

import com.hcmute.starter.model.entity.CartEntity;
import com.hcmute.starter.model.entity.CartItemEntity;
import com.hcmute.starter.model.entity.ProductEntity;
import com.hcmute.starter.model.entity.UserEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Component
@Service
public interface CartService {
    CartEntity saveCart(CartEntity cart);
    CartEntity getCartByUid(UserEntity uid);
    List<CartItemEntity> getCartItem(CartEntity cart);
    void deleteCartById(int id);
    int calCartTotal(CartEntity cart);
    CartItemEntity saveCartItem(CartItemEntity cartItem);
    CartItemEntity getCartItemByPid(ProductEntity id);
    void deleteCartItem(int id);

}
