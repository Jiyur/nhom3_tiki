package com.hcmute.starter.service.Impl;

import com.hcmute.starter.model.entity.BrandEntity;
import com.hcmute.starter.repository.BrandRepository;
import com.hcmute.starter.service.BrandService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class BrandServiceImpl implements BrandService {
    private final BrandRepository brandRepository;
    @Override
    public BrandEntity findById(UUID id) {
        Optional<BrandEntity> brand = brandRepository.findById(id);
        if(brand.isEmpty())
            return null;
        return brand.get();
    }
    @Override
    public BrandEntity saveBrand(BrandEntity brand) {
        return brandRepository.save(brand);
    }
    @Override
    public List<BrandEntity> findAll(){
        return brandRepository.findAll();
    }
    @Override
    public void deleteBrand(UUID id){
        brandRepository.deleteById(id);
    }
}
