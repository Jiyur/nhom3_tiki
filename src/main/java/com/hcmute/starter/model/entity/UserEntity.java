package com.hcmute.starter.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import com.hcmute.starter.model.entity.ProductRating.ProductRatingCommentEntity;
import com.hcmute.starter.model.entity.ProductRating.ProductRatingEntity;
import com.hcmute.starter.model.entity.ProductRating.ProductRatingLikeEntity;
import com.hcmute.starter.model.entity.userAddress.AddressEntity;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@RestResource(exported = false)
@Entity
@Table(name = "\"users\"")
public class UserEntity {
    @Id
    @Column(name = "user_id")
    @GeneratedValue(
            generator = "UUID"
    )
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    private UUID id;
    @Column(name = "\"full_name\"")
    private String fullName;

    @Column(name = "\"email\"")
    private String email;

    @JsonIgnore
    @Column(name = "\"password\"")
    private String password;

    @Column(name = "\"gender\"")
    private String gender;

    @Column(name = "\"nick_name\"")
    private String nickName;

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    @Column(name = "\"phone\"")
    private String phone;
    @JsonIgnore
    @OneToMany(mappedBy = "user",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<AddressEntity> address;

    @Column(name = "\"birth_day\"")
    private LocalDateTime birth_day;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "\"country_id\"",referencedColumnName = "country_id")
    private CountryEntity country;
    @JsonIgnore
    @OneToMany(mappedBy = "user",targetEntity = ProductRatingEntity.class,fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<ProductRatingEntity> productRating;
    @JsonIgnore
    @OneToMany(mappedBy = "user",targetEntity = ProductRatingLikeEntity.class,fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<ProductRatingLikeEntity> productRatingLiked;
    @Column(name = "\"img\"")
    private String img;
    @JsonIgnore
    @OneToMany(mappedBy = "user",targetEntity = ProductRatingCommentEntity.class,cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<ProductRatingCommentEntity> commentList;

    private boolean status;
    private boolean active;

    private LocalDateTime createAt;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "\"favorite_list\"",
            joinColumns = @JoinColumn(name = "\"user_id\""),
            inverseJoinColumns = @JoinColumn(name = "\"product_id\""))
    private List<ProductEntity> favoriteProducts;

    public LocalDateTime getCreateAt() {
        return createAt;
    }

    public void setCreateAt(LocalDateTime createAt) {
        this.createAt = createAt;
    }

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "\"user_role\"", joinColumns = @JoinColumn(name = "\"user_id\""), inverseJoinColumns = @JoinColumn(name = "\"role_id\""))
    private Set<RoleEntity> roles;
    @JsonIgnore
    @OneToOne(mappedBy = "user",targetEntity = com.hcmute.starter.model.entity.CartEntity.class,cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private CartEntity cart;

    public CartEntity getCart() {
        return cart;
    }

    public void setCart(CartEntity cart) {
        this.cart = cart;
    }

    public UserEntity() {
    }
    public UserEntity(String fullName, String email, String password) {
        this.email = email;
        this.fullName = fullName;
        this.password = password;
        this.active = false;
        this.status = true;
        this.createAt=LocalDateTime.now(ZoneId.of("GMT+07:00"));
    }
    public UserEntity(String phone,String password){
        this.phone = phone;
        this.password = password;
        this.active = false;
        this.status = true;
        this.createAt=LocalDateTime.now(ZoneId.of("GMT+07:00"));

    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<AddressEntity> getAddress() {
        return address;
    }

    public void setAddress(List<AddressEntity> address) {
        this.address = address;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Set<RoleEntity> getRoles() {
        return roles;
    }

    public void setRoles(Set<RoleEntity> roles) {
        this.roles = roles;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public LocalDateTime getBirth_day() {
        return birth_day;
    }

    public void setBirth_day(LocalDateTime birth_day) {
        this.birth_day = birth_day;
    }

    public CountryEntity getCountry() {
        return country;
    }

    public void setCountry(CountryEntity country) {
        this.country = country;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public List<ProductEntity> getFavoriteProducts() {
        return favoriteProducts;
    }

    public void setFavoriteProducts(List<ProductEntity> favoriteProducts) {
        this.favoriteProducts = favoriteProducts;
    }
}
