package com.hcmute.starter.mapping;

import com.hcmute.starter.model.entity.BrandEntity;
import com.hcmute.starter.model.entity.CategoryEntity;
import com.hcmute.starter.model.entity.ProductEntity;
import com.hcmute.starter.model.payload.request.AddNewProductRequest;

import java.util.Optional;

public class ProductMapping {
    public static ProductEntity addProductToEntity(AddNewProductRequest addNewProductRequest, CategoryEntity category, BrandEntity brand){
        return new ProductEntity(brand,category,addNewProductRequest.getName(),addNewProductRequest.getPrice(),addNewProductRequest.getDescription(),addNewProductRequest.getInventory());
    }
}
