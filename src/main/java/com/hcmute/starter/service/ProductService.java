package com.hcmute.starter.service;

import com.hcmute.starter.model.entity.ProductEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Component
@Service
public interface ProductService {
    List<ProductEntity> findAllProduct();

    ProductEntity saveProduct(ProductEntity product);

    ProductEntity findById(UUID id);

    void deleteProduct(UUID id);

    void saveListImageProduct(List<String> listUrl, ProductEntity product);

    void deleteListImgProduct(ProductEntity product);

    void addAttribute(ProductEntity product, Integer attributeId);

    void deleteAttribute(ProductEntity product, Integer attributeId);
}
