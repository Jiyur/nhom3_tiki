package com.hcmute.starter.mapping;

import com.hcmute.starter.model.entity.CategoryEntity;
import com.hcmute.starter.model.payload.request.ProductRequest.AddCategoryRequest;
import com.hcmute.starter.model.payload.request.ProductRequest.UpdateCategoryRequest;

public class CategoryMapping {
    public static CategoryEntity requestToEntity(AddCategoryRequest request)
    {
        CategoryEntity category = new CategoryEntity();
        category.setName(request.getName());
        category.setParent(request.getParent());
        return category;
    }
    public static CategoryEntity updateReqToEntity(CategoryEntity category, UpdateCategoryRequest request)
    {
        category.setName(request.getName());
        category.setParent(request.getParent());
        return category;
    }
}
