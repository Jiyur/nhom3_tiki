package com.hcmute.starter.service.Impl;

import com.hcmute.starter.model.entity.CartEntity;
import com.hcmute.starter.model.entity.CartItemEntity;
import com.hcmute.starter.model.entity.ProductEntity;
import com.hcmute.starter.model.entity.UserEntity;
import com.hcmute.starter.repository.CartItemRepository;
import com.hcmute.starter.repository.CartRepository;
import com.hcmute.starter.service.CartService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor
public class CartServiceImpl implements CartService {
    final CartItemRepository cartItemRepository;
    final CartRepository cartRepository;
    @Override
    public CartEntity saveCart(CartEntity cart) {
        return cartRepository.save(cart);
    }

    @Override
    public CartEntity getCartByUid(UserEntity user) {
        Optional<CartEntity> cart = cartRepository.findByUserAndStatus(user,true);
        if (cart.isEmpty())
            return null;
        return cart.get();
    }

    @Override
    public List<CartItemEntity> getCartItem(CartEntity cart) {
        return null;
    }

    @Override
    public void deleteCartById(int id) {

    }
    @Override
    public int calCartTotal(CartEntity cart) {
        return 0;
    }

    @Override
    public CartItemEntity saveCartItem(CartItemEntity cartItem) {
        return cartItemRepository.save(cartItem);
    }

    @Override
    public CartItemEntity getCartItemByPid(ProductEntity id) {

        Optional<CartItemEntity> cartItem = cartItemRepository.findByProduct(id);
        if (cartItem.isEmpty())
            return null;
        return cartItem.get();
    }

    @Override
    public void deleteCartItem(int id) {
        cartItemRepository.deleteById(id);
    }
}
