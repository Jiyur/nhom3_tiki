package com.hcmute.starter.service.Impl;

import com.hcmute.starter.model.entity.AttributeEntity;
import com.hcmute.starter.repository.AttributeRepository;
import com.hcmute.starter.service.AttributeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor
public class AttributeServiceImpl implements AttributeService {
    private final AttributeRepository attributeRepository;
    @Override
    public List<AttributeEntity> findAll(){
        return attributeRepository.findAll();
    }
    @Override
    public AttributeEntity findById(Integer id) {
        Optional<AttributeEntity> attribute = attributeRepository.findById(id);
        if(attribute.isEmpty())
            return null;
        return attribute.get();
    }
    @Override
    public AttributeEntity saveAttribute(AttributeEntity attributeEntity) {
        return attributeRepository.save(attributeEntity);
    }
    @Override
    public void deleteAttribute(Integer id){
        attributeRepository.deleteById(id);
    }
}
