package com.hcmute.starter.model.entity;


import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@RestResource(exported = false)
@Table(name = "\"category\"")
public class CategoryEntity {
    @Id
    @Column(name = "\"category_id\"")
    @GeneratedValue(
            generator = "UUID"
    )
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    private UUID id;
    @Column(name = "\"category_name\"", nullable = false)
    private String name;
    @Column(name = "\"category_parent\"", nullable = false)
    private String parent;
    @OneToMany(mappedBy = "productCategory",cascade = CascadeType.ALL)
    private List<ProductEntity> listProduct;

    @OneToMany(mappedBy = "attributeCategory",cascade = CascadeType.ALL)
    private List<AttributeEntity> attributeEntities;
    public CategoryEntity() {
    }

    public CategoryEntity(String name, String parent) {
        this.name = name;
        this.parent = parent;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", parent='" + parent + '\'' +
                '}';
    }
}