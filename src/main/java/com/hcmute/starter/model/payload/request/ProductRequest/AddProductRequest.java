package com.hcmute.starter.model.payload.request.ProductRequest;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Getter
@Setter
@NoArgsConstructor
public class AddProductRequest {
    @NotEmpty(message = "Tên người bán không được để trống")
    String shopName;
    @NotNull(message = "Category không được để trống")
    int categoryId;
    @NotEmpty(message = "Tên sản phẩm không được để trống")
    String productName;
    @NotNull(message = "Giá bán không được để trống")
    @Min(value=0,message = "Giá sản phẩm phải lớn hơn 0")
    int price;
    @NotEmpty(message = "Chi tiết sản phẩm không được để trống")
    String description;
    @NotNull(message = "Số lượng tồn kho không được để trống")
    @Min(value = 0,message = "Số lượng tồn kho phải lớn hơn 0")
    int inventory;
}
