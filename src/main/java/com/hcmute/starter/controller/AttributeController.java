package com.hcmute.starter.controller;

import com.hcmute.starter.model.entity.AttributeEntity;
import com.hcmute.starter.model.entity.CategoryEntity;
import com.hcmute.starter.model.payload.SuccessResponse;
import com.hcmute.starter.model.payload.request.ProductRequest.AddAttributeRequest;
import com.hcmute.starter.service.AttributeService;
import com.hcmute.starter.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/attribute")
@RequiredArgsConstructor
public class AttributeController {
    private final AttributeService attributeService;
    private final CategoryService categoryService;

    @GetMapping("/all")
    public ResponseEntity<SuccessResponse> getAllAttribute(){
        List<AttributeEntity> listAttribute = attributeService.findAll();
        SuccessResponse response=new SuccessResponse();
        if(listAttribute.size() == 0){
            response.setMessage("List Attribute is Empty");
            response.setSuccess(false);
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        response.setMessage("Query Attribute Successfully");
        response.setSuccess(true);
        response.setStatus(HttpStatus.OK.value());
        response.getData().put("List Attribute", listAttribute);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<SuccessResponse> getAttributeByID(@PathVariable Integer id){
        AttributeEntity attribute = attributeService.findById(id);
        SuccessResponse response=new SuccessResponse();
        if(attribute == null){
            response.setMessage("Attribute Not found");
            response.setSuccess(false);
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        response.setMessage("Query Attribute Successfully");
        response.setSuccess(true);
        response.setStatus(HttpStatus.OK.value());
        response.getData().put("Attribute", attribute);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @PostMapping("/insert")
    public ResponseEntity<SuccessResponse> insertAttribute(@RequestBody AddAttributeRequest attributeRequest){
        CategoryEntity category = categoryService.findById(attributeRequest.getCategoryId());
        if(category == null){
            SuccessResponse response=new SuccessResponse();
            response.setMessage("Category not found");
            response.setSuccess(false);
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        AttributeEntity attribute = new AttributeEntity(category,attributeRequest.getName(),attributeRequest.getValue(),attributeRequest.getSuffix());
        attributeService.saveAttribute(attribute);
        SuccessResponse response=new SuccessResponse();
        response.setMessage("Insert Attribute Successfully");
        response.setSuccess(true);
        response.setStatus(HttpStatus.OK.value());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @PutMapping("/update/{id}")
    public ResponseEntity<SuccessResponse> UpdateAttribute(@PathVariable Integer id,@RequestBody AddAttributeRequest attributeRequest){
        AttributeEntity attribute = attributeService.findById(id);
        if(attribute == null){
            SuccessResponse response=new SuccessResponse();
            response.setMessage("Attribute not found");
            response.setSuccess(false);
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        CategoryEntity category = categoryService.findById(attributeRequest.getCategoryId());
        if(category == null){
            SuccessResponse response=new SuccessResponse();
            response.setMessage("Category not found");
            response.setSuccess(false);
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        attribute.setName(attributeRequest.getName());
        attribute.setSuffix(attributeRequest.getSuffix());
        attribute.setValue(attributeRequest.getValue());
        attribute.setAttributeCategory(category);
        attributeService.saveAttribute(attribute);
        SuccessResponse response=new SuccessResponse();
        response.setMessage("Update Attribute Successfully");
        response.setSuccess(true);
        response.setStatus(HttpStatus.OK.value());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<SuccessResponse> deleteAttributeById(@PathVariable Integer id){
        AttributeEntity attribute = attributeService.findById(id);
        SuccessResponse response=new SuccessResponse();
        if(attribute == null){
            response.setMessage("Attribute Not found");
            response.setSuccess(false);
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        attributeService.deleteAttribute(id);
        response.setMessage("Attribute is deleted");
        response.setSuccess(true);
        response.setStatus(HttpStatus.OK.value());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
