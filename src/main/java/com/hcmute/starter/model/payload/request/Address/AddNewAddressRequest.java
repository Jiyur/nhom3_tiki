package com.hcmute.starter.model.payload.request.Address;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@Setter
@Getter

public class AddNewAddressRequest {
    @NotEmpty(message = "Họ và tên không được để trống")
    private String fullName;
    private String companyName;
    @NotEmpty(message = "Số điện thoại không được để trống")
    private String phone;
    @NotEmpty(message = "Tỉnh thành không được để trống")
    private String province;
    @NotEmpty(message = "Quận huyện không được để trống")
    private String district;
    @NotEmpty(message = "Phường xã không được để trống")
    private String commune;
    @NotEmpty(message = "Thông tin chi tiết không được để trống")
    private String addressDetail;
    @NotNull(message = "Vui lòng chọn loại địa chỉ")
    private int addressType;
}
