package com.hcmute.starter.repository;

import com.hcmute.starter.model.entity.CategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.List;
import java.util.UUID;
@EnableJpaRepositories
public interface CategoryRepository extends JpaRepository<CategoryEntity, UUID>  {
    List<CategoryEntity> findByName(String categoryName);

}
